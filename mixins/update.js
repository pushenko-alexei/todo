import {mapState, mapMutations} from 'vuex'
export default{
    computed:{
        ...mapState({
           items: state => state.list
        }),
        id(){
            return this.$route.params.id
        },

        item(){
            return this.items.filter(item => item.id == this.id)[0]
        }
    },

    methods: {
        ...mapMutations({
            remove: 'REMOVE_TODO_ITEM'
        }),
        update(){
            this.$auth.$storage.setLocalStorage('list', this.items)
        },

        removeTask(item){
            let bool = confirm('Вы точно хотите удалить задачу?')
            bool && this.remove(item)
            this.$nuxt.$router.push({path: '/list'})
            this.update()
        },
    }
}