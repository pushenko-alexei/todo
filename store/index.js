import Vue from 'vue'
import Vuex from 'vuex'
import { v4 as uuidv4 } from 'uuid';
Vue.use(Vuex)

const store = () => new Vuex.Store({

  state: {
    list: [],
    filterCategory: 'all',
    filterCheck: null
  },

  mutations: {
        SET_ITEMS(state, list){
            state.list = list
        },

        ADD_TODO_ITEM(state, item){
            state.list.push(item)
        },

        REMOVE_TODO_ITEM(state, item){
            let idx = state.list.indexOf(item)
            state.list.splice(idx, 1)
            this.$auth.$storage.setLocalStorage('list', state.list)
        },

        SET_FILTER_CATEGORY(state, filter){
            state.filterCategory = filter
        },
        SET_FILTER_CHECK(state, filter){
            state.filterCheck = filter
        }
   },

    actions:{
        setTodoList({commit}){
            let data = getStartedList();
            let storage;
            if(this.$auth.$storage.getLocalStorage('list')){
                console.log(111)
                storage = this.$auth.$storage.getLocalStorage('list') 
            }else{
                this.$auth.$storage.setLocalStorage('list', data)   
            }
            commit('SET_ITEMS', storage)
        },
    }
})

function getStartedList(){
    let data = [
        {
            checked: true,
            name: 'Имя 1',
            category: 'main',
        },
        {
            checked: false,
            name: 'Имя 2',
            category: 'main',
        },
        {
            checked: true,
            name: 'Имя 3',
            category: 'buy',
        },
        {
            checked: true,
            name: 'Имя 4',
            category: 'work',
        },
        {
            checked: false,
            name: 'Имя 5',
            category: 'buy',
        },
        {
            checked: true,
            name: 'Имя 6',
            category: 'main',
        },
        {
            checked: true,
            name: 'Имя 7',
            category: 'work',
        }
    ]
   
    data.forEach((item, idx) => {
        item.id = uuidv4();
        item.description = 'text'
    })
    return data
}

export default store